// Define um recurso de instância do Google Compute Engine
resource "google_compute_instance" "vm_instance" {
  name         = var.vm_name
  machine_type = var.machine_type

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-focal-v20220712"
    }
  }

  metadata = {
    user-data = file("${path.module}/cloudconfig/cloudconfig.yml")
  }

  network_interface {
    network = "default"
    access_config {

    }
  }

  tags = ["http-server", "ssh"]
}