// Define uma variável de saída para exibir o endereço IP externo da instância
output "ip" {
  value = google_compute_instance.vm_instance.network_interface.0.access_config.0.nat_ip
}